package demo.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller // This means that this class is a Controller
@RequestMapping(path = "/demo") // This means URL's start with /demo (after Application path)
public class DemoMustache {

	@GetMapping(path = "mustache")
	/**
	 * Un exemple
	 * @param sexe "M" ou "F"
	 * @param prenoms une liste de prénoms
	 * @param model initialisé par Spring
	 * @return le nom de la vue
	 */
	public String exemplesMustache(
		@RequestParam(required = false) String sexe,
		@RequestParam(name="prenom", defaultValue="") List<String> prenoms, Model model
	) {
		model.addAttribute("sexe", gender(sexe));
		model.addAttribute("prenoms", prenoms);
		return "demoMustache";
	}

	private String gender(String sexe) {
		switch(sexe) {
			case "M":
				return "Monsieur";
			case "F":
				return "Madame";
			default:
				return "Inconnu";
		}
	}
}
