package demo.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import demo.form.SimpleBeanParam;
import demo.form.ValidatingBeanParam;

@Controller // This means that this class is a Controller
@RequestMapping(path = "/form") // This means URL's start with /form (after Application path)
public class FormController {
	/**
	 * Afficher le forumaire sans validation
	 * @param beanParam Spring "injecte" un nouveau SimpleBeanParam initialisé avec ses valeurs par défaut
	 * @return le nom de la vue à afficher (formulaire de saisie)
	 */
	@GetMapping(path = "beanParam")
	public String montreLeFormulaireSansvalidation(@ModelAttribute("beanParam") SimpleBeanParam beanParam) {
		// @ModelAttribute enregistre automatiquement l'objet dans le modèle
		// On aurait pu faire :
		//model.addAttribute("beanParam", new SimpleBeanParam()); // Valeurs par défaut		
		return "beanForm"; // On affiche la vue 'beanForm.html'
	}

	/**
	 * Appelé par le "Submit" du formulaire "beanForm" (méthode POST)
	 * @param beanParam Spring "injecte" un SimpleBeanParam initialisé suivant les données saisies dans le formulaire
	 * @return le nom de la vue à afficher (affiche les données saisies)
	 */
	@PostMapping(path = "beanParam")
	public String traiteLeFormulaireSansValidation(
		@ModelAttribute("beanParam") 
		SimpleBeanParam beanParam
	) {
		// on affiche la vue qui montre les données saisies
		return "showFormData";
	}

	/**
	 * Afficher le forumaire avec validation
	 * @param beanParam Spring "injecte" un nouveau validatingBeanParam initialisé avec ses valeurs par défaut
	 * @return le nom de la vue à afficher (formulaire de saisie)
	 */
	@GetMapping(path = "validBeanParam")
	public String montreLeFormulaireAvecvalidation(@ModelAttribute("beanParam") ValidatingBeanParam beanParam) {
		return "beanForm";
	}

	/**
	 * Appelé par le "Submit" du formulaire "beanForm"
	 * @param beanParam Spring "injecte" un ValidatingBeanParam initialisé suivant les données saisies dans le formulaire
	 * @param validation les erreurs de validation
	 * @return le nom de la vue à afficher (affiche les données saisies, ou revenir sur le formulaire)
	 */
	@PostMapping(path = "validBeanParam")
	public String traiteLeFormulaireAvecValidation(
			@Valid @ModelAttribute("beanParam") 
			ValidatingBeanParam beanParam, 
			BindingResult validation,
			Model model) {
		String view;
		model.addAttribute("validation", validation);
		validation.getFieldErrors().forEach(e -> System.out.println(e.getField() + " " + e.getDefaultMessage()));
		if (!validation.hasErrors()) { // Pas d'erreur de saisie
			view = "showFormData"; // on montre les données saisies			
		} else { // En cas d'erreur de saisie
			view = "beanForm"; // On revient vers le formulaire en montrant les erreurs de saisie
		}
		return view;
	}
}
