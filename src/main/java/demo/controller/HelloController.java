package demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import demo.session.SessionData;
import lombok.extern.slf4j.Slf4j;

@Controller // This means that this class is a Controller
@RequestMapping(path = "/hello") // This means URL's start with /hello (after Application path)
@Slf4j
public class HelloController {
	
	@Autowired // Une variable de session, qui provoquera l'émission du cookie de session
	SessionData mySession;

	// On affichera par défaut la page 'hello.html'
	private static final String DEFAULT_VIEW = "hello";
	
	@GetMapping(path = "noParam")
	public String sayHello(Model model) {
		model.addAttribute("message", "World");
		return DEFAULT_VIEW;
	}

	@GetMapping(path = "requestParam")
	// cf. https://www.baeldung.com/spring-request-param
	public String sayHelloTo(
		@RequestParam(required = false) String name, 
		Model model
	) {
		model.addAttribute("message", name);
		return DEFAULT_VIEW;
	}

	@GetMapping(path = "defaultValue")
	public String sayHelloToDefault(
		@RequestParam(defaultValue = "Inconnu") 
		String name, 
		Model model
	) {
		model.addAttribute("message", name);
		return DEFAULT_VIEW;
	}

	@GetMapping(path = "formulaire")
	public String montreLeFormulaire() {
		log.debug("Appel par méthode GET, on montre le formulaire");
		return "formulaireHello";
	}

	@PostMapping(path = "formulaire")
	public String traiteLesParametresDuFormulaire(@RequestParam String sexe, Model model) {
		log.debug("Appel par méthode POST, on traite les paramètres reçus");
		String value = "Unknown Gender";
		switch (sexe) {
			case "M":
				value = "Sir";
				break;
			case "F":
				value = "Madam";
				break;
		}
		model.addAttribute("message", value);
		return DEFAULT_VIEW;
	}

	@GetMapping(path = "header")
	// On accède à  un en-tête de la requête HTTP
	public String showHeader(@RequestHeader("User-Agent") String agent,  Model model) {
		model.addAttribute("message", "Vous utilisez le navigateur: " + agent);
		return DEFAULT_VIEW;
	}

	@GetMapping(path = "cookie")
	// On accède à  un cookie transmis dans la requête HTTP
	// Ici, on affiche le "cookie de session", créé automatiquement par Spring quand on utilise les variables de session
	public String showCookie(@CookieValue(name="SESSION", required = false) String idSession, Model model) {
		log.debug("Session créée le {}", mySession.getCreatedAt());

		model.addAttribute("message", "session n° " + idSession);
		return DEFAULT_VIEW;
	}

	@GetMapping(path = "to/{name}")
	// le paramètre est extrait du chemin d'accès
	public String showPathParam(@PathVariable String name, Model model) {
		model.addAttribute("message", name);
		return DEFAULT_VIEW;
	}
}
