package demo.form;

import java.math.BigInteger;
import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Un objet java qui sera affiché / modifié par un formulaire HTML
 * @see demo.controller.HelloController le contrôleur 
 * cf. La vue (src/main/resources/templates/formulaireHello.mustache)
 * Définit des règles de validation de données, les erreurs éventuelles seront afichées dans la vue
 * @See https://www.baeldung.com/javax-validation
 */
@Data // https://projectlombok.org/features/Data
public class ValidatingBeanParam {
	// Contraintes de taille
	@Size(min = 5, max = 25)
	// Format défini par une 'expression régulière' : cf. https://www.vogella.com/tutorials/JavaRegularExpressions/article.html
	@Pattern(regexp = "[A-Z][a-z]+", message = "Une majuscule suivie de minuscules")	
	private String nom = "Exemple";

	@NotEmpty 
	@Email // Doit avoir la forme d'une adresse email
	private String email = "Nom.Prenom@somewhere.com";
	
	@NotNull
	@Past // La date doit être dans le passé 
	// Le format attendu pour les champs html <input type='date'>
	@DateTimeFormat(pattern = "yyyy-MM-dd")	
	private LocalDate naissance = LocalDate.now().minusDays(1);
	
	@Positive(message = "Le nombre doit être positif")
	@Max(value = 10000)
	private BigInteger salaire = BigInteger.valueOf(1234);
}
