package demo.form;

import java.math.BigInteger;
import java.time.LocalDate;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Un objet java qui sera affiché / modifié par un formulaire HTML
 * @see demo.controller.HelloController le contrôleur
 * cf. La vue (src/main/resources/templates/formulaireHello.html)
 */
@Data // https://projectlombok.org/features/Data
public class SimpleBeanParam  {
	// on peut définir des valeurs par défaut pour les différentes propriétés
	private String nom = "Votre nom";

	private String email = "Votre.Email@somewhere.com";
	
	// Le format attendu par les champs html <input type='date'>
	@DateTimeFormat(pattern = "yyyy-MM-dd")	
	private LocalDate naissance = LocalDate.now();
	
	private BigInteger salaire = new BigInteger("1234");
	
}
