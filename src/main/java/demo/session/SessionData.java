package demo.session;


import java.io.Serializable;
import java.time.LocalDateTime;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import lombok.Data;

/**
 * Un exemple de variable de session
 */
@Component
@SessionScope
@Data // https://projectlombok.org/features/Data
public class SessionData implements Serializable {
	private static final long serialVersionUID = 7445124270947194875L;

	// On mémorise pour chaque utilisateur le moment où la session a été initialisée
	private final LocalDateTime createdAt = LocalDateTime.now();
}
