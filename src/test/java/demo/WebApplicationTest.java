package demo;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class WebApplicationTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void montreLaPageDAccueil() throws Exception {
		mockMvc.perform(get("/")). // On exécute la requête HTTP
			andDo(print()). // On imprime le résultat
			andExpect(status().isOk()). // On vérifie le code de retour HTTP
			andExpect(forwardedUrl("index.html")); // On vérifie qu'on a été redirigé vers la page d'accueil
	}

	@Test
	public void helloMarche() throws Exception {
		mockMvc.perform(get("/hello/noParam")).
			andDo(print()).
			andExpect(status().isOk()).
			// On vérifie que la réponse a le contenu attendu
			andExpect(content().string(containsString("Hello <em>World</em>")));
	}

	@Test
	public void helloMarcheAvecParametre() throws Exception {
		// On exécute une requête HTTP en passant un paramètre
		mockMvc.perform(get("/hello/requestParam").param("name", "Bastide")).
			andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().string(containsString("Hello <em>Bastide</em>")));
	}

}